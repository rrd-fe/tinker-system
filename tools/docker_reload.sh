#!/bin/bash

base_dir=$(cd "$(dirname "$0")";pwd)
system_dir="${base_dir}/.."
run_dir="/usr/app/leekserver"

echo "切换到应用根目录"
cd ${run_dir}

echo "执行  pm2-runtime"
pm2-runtime pm2.json --env production

