#!/bin/sh

# mysql初始化脚本，创建数据库，表，导入初始数据

base_dir=$(cd "$(dirname "$0")";pwd)

# 创建DB放在 .sql 文件里，貌似不行啊，先放到 shell 里吧
mysql -uroot -e "create database tinker_system;"

mysql -uroot tinker_system < ${base_dir}/tinker_system_db_init.sql

