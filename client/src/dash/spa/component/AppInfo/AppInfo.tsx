/**
 * 展示APP的一些信息，在相关页面上，方便查看当前所处APP和跳转到详情页
 */

import * as React from 'react';
import * as qs from 'qs';
import {withRouter, RouteComponentProps, Route} from 'react-router';
import { Spin, Button, message, Modal, Form, Input, 
    Select, Table, Card, DatePicker, Row, Col } from 'antd';

import { IMetricsSummary, getPackageListMetrics, IPackageListMetricsItem, IPackageListMetrics, IRollbackLogItem, getPackageRollbackList, IRollbackListResult } from 'dash/spa/service/app';
import { IExistApp, IPackage, PackageStatusMap, PackageStatus, PackageDisablePatch, PackageDisablePatchText, PackageForceUpdate, PackageForceUpdateText, PlatformText } from 'dash/spa/interface/app';
import * as moment from 'moment';
import history from 'common/ui/history/history';

interface IProps{
    app: IExistApp | null;
}

import './AppInfo.scss';

export default class AppInfo extends React.Component<IProps>{

    constructor(props: IProps){
        super(props);

        this.showAppDetail = this.showAppDetail.bind( this );
    }

    showAppDetail(){
        if( ! this.props.app ){
            return;
        }
        history.push(`/dash/apps/detail?appId=${this.props.app.id}`);
    }

    render(){

        const { app } = this.props;

        if( ! app ){
            return null;
        }

        return (
            <div className="com-app-info">
                <div className="item">
                    <span className="label">APP名：</span>
                    <span className="value">
                        <Button onClick={ this.showAppDetail } title="查看APP详情" type="primary">{ app.name }</Button>
                    </span>
                </div>
                <div className="item">
                    <span className="label">平台：</span>
                    <span className="value">{ PlatformText[app.platform] }</span>
                </div>
            </div>
        );
    }
}