/**
 * 某个APP下，RN的下载、激活、回滚统计汇总页面
 */

import * as React from 'react';
import * as qs from 'qs';
import {withRouter, RouteComponentProps, Route} from 'react-router';
import { Spin, Button, message, Modal, Form, Input, 
    Select, Table, Card, DatePicker, Row, Col } from 'antd';

import { IMetricsSummary, getPackageListMetrics, IPackageListMetricsItem, IPackageListMetrics, getAppDetail } from 'dash/spa/service/app';
import { IExistApp, IPackage, PackageStatusMap, PackageStatus, PackageDisablePatch, PackageDisablePatchText, PackageForceUpdate, PackageForceUpdateText } from 'dash/spa/interface/app';
import * as moment from 'moment';

import AppInfo from 'dash/spa/component/AppInfo/AppInfo';

import './AppMetrics.scss';

const FormItem = Form.Item;

interface IState{
    isLoad: boolean;
    app: IExistApp | null;
    list: Array<IPackageListMetricsItem>;
    totalActive: number;
}

export default class AppMetrics extends React.Component<RouteComponentProps, IState>{

    private appId: number;
    private appVersionRef = React.createRef<Input>();

    private columns: Array<object>;

    constructor(props: RouteComponentProps){
        super(props);

        this.state = {
            isLoad: false,
            app: null,
            list: [],
            //总激活量
            totalActive: 0,
        };

        this.columns = [
            {
                title: 'RN版本号',
                dataIndex: 'packageVersion',
                key: 'packageVersion',
            },
            {
                title: '激活',
                dataIndex: 'active',
                key: 'active',
                render: (active: number, row: IPackageListMetricsItem) => {
                    return (
                    <div>
                        <div>激活量：{active}</div>
                        <div>激活占比：{row.rate}%</div>
                    </div>);
                }
            },
            {
                title: '回滚次数',
                dataIndex: 'rollback',
                key: 'rollback',
                render: (count: number, row: IPackageListMetricsItem) => {
                    if( count > 0 ){
                        return <Button title="点击查看详细回滚错误日志" type="danger" onClick={ this.showRollbackLogList.bind(this, row.packageVersion)}>{ count }</Button>;
                    }else{
                        return count;
                    }
                }
            },
            {
                title: '下载次数统计',
                key: 'download',
                children:[
                    {
                        title: '总次数',
                        dataIndex: 'totalDownload',
                    },
                    {
                        title: '全量包次数',
                        dataIndex: 'fullDownload',
                    },
                    {
                        title: '增量包次数',
                        dataIndex: 'patchDownload',
                    }
                ]
            }
        ];

        this.doSearch = this.doSearch.bind( this );
    }

    componentDidMount(){
        document.title = `RN数据统计`;

        const searchConf = qs.parse(location.search.substring(1));
        this.appId = parseInt(searchConf.appId, 10);
        if( isNaN(this.appId)){
            this.setState({
                isLoad: false,
            });
            message.error(`页面参数错误，appId非法！`);
            return;
        }

        getAppDetail(this.appId)
        .then( (app) => {
            this.setState({
                app,
            });
        })
        .catch( (err) => {
            message.error(err.message);
        });
    }

    doSearch(e: React.FormEvent){
        e.preventDefault();

        const appVersion = this.appVersionRef.current!.input.value;

        this.setState({
            isLoad: true,
        });

        getPackageListMetrics(this.appId, appVersion)
        .then( ({ list, message: out}: IPackageListMetrics) => {
            let totalActive = list.reduce( (prev: number, obj) => {
                return prev + obj.active;
            }, 0);
            list.forEach( (obj) => {
                obj.rate = '--';
                if( totalActive > 0 ){
                    obj.rate = String(Math.floor( obj.active * 100 / totalActive));
                }
            });
            this.setState({
                isLoad: false,
                list,
                totalActive,
            });
            if( out.download ){
                message.error(out.download);
            }
            if( out.active ){
                message.error(out.active);
            }
            if( out.rollback ){
                message.error(out.rollback);
            }
        })
        .catch( (err: Error) => {
            this.setState({
                isLoad: false,
                list: [],
            });
            message.error(err.message);
        });
    }

    /**
     * 跳转到该RN版本对应的回滚日志列表页
     * @param packageVersion 
     */
    showRollbackLogList(packageVersion: number){
        const appVersion = this.appVersionRef.current!.input.value;
        this.props.history.push(`/dash/apps/rollbackLogList?appId=${this.appId}&appVersion=${encodeURIComponent(appVersion)}&packageVersion=${packageVersion}`);
    }

    renderSearchForm(){
        return (
            <Form className="search-form" layout="inline" onSubmit={this.doSearch}>
                <FormItem label="native版本号">
                    <Input
                        type="text"
                        ref={ this.appVersionRef }
                        style={{ width: '65%', marginRight: '3%' }}
                    />
                </FormItem>
                <FormItem>
                    <Button type="primary" htmlType="submit">搜索</Button>
                </FormItem>
            </Form>
        );
    }

    renderSummary(){

        return (
            <div>
                <Table 
                bordered
                rowKey="packageVersion"
                dataSource={this.state.list} 
                columns={this.columns} />
            </div>
        );
    }

    render(){
        return (
            <div className="page-metrics">
                <h1>RN统计数据</h1>
                <AppInfo app={ this.state.app } />
                { this.renderSearchForm() }
                <Spin size="large" spinning={this.state.isLoad}>
                    { this.renderSummary() }
                </Spin>
            </div>
        );
    }
}

