/**
 * 查询某个RN版本对应的回滚日志列表
 */

import * as React from 'react';
import * as qs from 'qs';
import {withRouter, RouteComponentProps, Route} from 'react-router';
import { Spin, Button, message, Modal, Form, Input, 
    Select, Table, Card, DatePicker, Row, Col } from 'antd';

import { IMetricsSummary, getPackageListMetrics, IPackageListMetricsItem, IPackageListMetrics, IRollbackLogItem, getPackageRollbackList, IRollbackListResult } from 'dash/spa/service/app';
import { IExistApp, IPackage, PackageStatusMap, PackageStatus, PackageDisablePatch, PackageDisablePatchText, PackageForceUpdate, PackageForceUpdateText } from 'dash/spa/interface/app';
import * as moment from 'moment';

import AppInfo from 'dash/spa/component/AppInfo/AppInfo';

import './RollbackLogList.scss';

interface IState{
    isLoad: boolean;
    app: IExistApp | null;
    list: Array<IRollbackLogItem>;
}

export default class RollbackLogList extends React.Component<RouteComponentProps, IState>{

    private appId: number;
    private appVersion: string;
    private packageVersion: number;

    private columns: Array<object>;

    constructor(props: any){
        super(props);

        this.state = {
            isLoad: false,
            app: null,
            list: [],
        };

        this.columns = [
            {
                title: 'native版本号',
                dataIndex: 'appVersion',
                key: 'appVersion',
                className: 'version-info',
            },
            {
                title: 'RN版本号',
                dataIndex: 'packageVersion',
                key: 'packageVersion',
                className: 'version-info',
            },
            {
                title: '升级前RN版本号',
                dataIndex: 'fromPackageVersion',
                key: 'fromPackageVersion',
                className: 'version-info',
            },
            {
                title: '错误信息',
                dataIndex: 'msg',
                key: 'msg',
                render: (msg: string) => {
                    return <pre className="error-info">{msg}</pre>
                }
            },
            {
                title: '额外数据',
                dataIndex: 'extra',
                key: 'extra',
                render: (extra: string) => {
                    return <pre className="error-info">{extra}</pre>
                }
            }
        ];
    }

    componentDidMount(){
        document.title = 'RN回滚日志列表';

        const searchConf = qs.parse(location.search.substring(1));
        this.appId = parseInt(searchConf.appId, 10);
        this.appVersion = (searchConf.appVersion || '').trim();
        this.packageVersion = parseInt(searchConf.packageVersion, 10);
        if( isNaN(this.appId) || ! this.appVersion || isNaN(this.packageVersion)){
            this.setState({
                isLoad: false,
            });
            message.error(`页面参数错误`);
            return;
        }

        this.fetchLogList();
    }

    fetchLogList(){
        this.setState({
            isLoad: true,
        });
        getPackageRollbackList(this.appId, this.appVersion, this.packageVersion)
        .then( ({app,list}: IRollbackListResult) => {
            this.setState({
                isLoad: false,
                app,
                list,
            });
        })
        .catch( (err) => {
            this.setState({
                isLoad: false,
                list: []
            });
            message.error(err.message);
        });
    }

    render(){
        return (
            <div className="page-rollback-log-list">
                <h1>RN回滚日志列表</h1>
                <AppInfo app={ this.state.app } />
                <Spin size="large" spinning={this.state.isLoad}>
                    <Table 
                        bordered
                        rowKey="fromPackageVersion"
                        dataSource={this.state.list} 
                        columns={this.columns} />
                </Spin>
            </div>
        );
    }
}