/**
 *
 * Created by Jess on 2018/7/13.
 */

'use strict';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import LoginForm from 'dash/ui/LoginForm/LoginForm';

import './index.scss';

class App extends React.Component{

    render(){
        return (
            <div className="page-login">
                <h1>登录 Tinker</h1>
                <LoginForm />
            </div>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById('app'));
