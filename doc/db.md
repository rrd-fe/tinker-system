# 数据库

系统使用的是 `mysql` 数据库，具体情况如下：

* `database`: `tinker_system`

具体表结构，在 `tools/tinker_system_db_init.sql` 文件中。

