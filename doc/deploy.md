# 部署

后台系统，支持 **docker** 部署 和 手动部署，推荐使用 **docker** 部署，简化各种环境问题。

## docker部署

docker pull rrdfe/leekserver:noahsys(0)    
docker pull dushaobin/leekserver:noahsys(0)      

注： docker image 名称可能会更改，括号内数字为发布的版本号，后续可能会更改默认是没有任何版本

启动docker

```
docker run -d \
    -p 9030:9030 \
    -p 3306:3306 \
    -v ~/packages:/usr/app/packages \
    dushaobin/leekserver
```

启动完成后可以通过 9030端口来访问 tinker system服务

## 手动部署

### 环境依赖

运行环境

* `mysql >= 5.6.x`: 主要数据都存储在mysql中
* `redis >= 3.x`: session存储在redis中
* `Nodejs@^10.7.0`: 后台使用 `nodejs` 开发，
* `pm2@latest`: 服务进程使用 `pm2` 管理
* `yarn@latest`: 服务使用 `yarn` 来安装npm包

编译依赖

* `leek-cli@latest`: 本项目基于 [leekjs](https://github.com/WE-FE-TEAM/leekjs) 开发，因此需要使用 [leek-cli](https://github.com/rrd-fe/leek-cli) 打包编译

### 安装依赖

```shell
npm install -g pm2
npm install -g yarn
npm install -g leek-cli
```

### 修改配置

tinker系统将后台配置和代码分离，统一将代码中涉及到的外部服务、密码相关配置项，放在 `~/.tinker.env` 文件中，如果使用 `tomcat` 启动tinker，那么配置文件就是 `/home/tomcat/.tinker.env`，文件示例内容如下：

```shell
# 生成的RN包的根目录
package_root=/home/tomcat/data/packages
# tinker服务运行日志根目录
log_root=/home/tomcat/data/logs
# mysq related config
mysql_host=localhost
mysql_port=3306
mysql_user=root
mysql_password=IKrOgDUWfy3zOpPv
# redis 配置
redis_host=127.0.0.1
redis_port=6379
redis_db=1
# cli命令行相关
cli_secret=nOcj2cWmIqQEL28Y
```

<s>手动部署需要修改server中，`mysql` `redis` 对应的相关配置。在生产环境中，建议以 `production` 来启动服务，因此，需要修改 **源代码** 中，`src/lib/config/config.production.js` 文件的 `config.mysql` `config.redis` 相关配置项。

`mysql`安装完之后，可以创建数据库，默认为 `tinker_system`，如果有修改，请更新 `tools/tinker_system_db_init.sql` 文件里对应的 **数据库** 名，然后导入 `tools/tinker_system_db_init.sql` 文件，在数据库中创建对应的表。

同时，需要修改 全量包和增量包 产出的根目录：`packageRoot`；后台日志的根目录： `logRoot`。</s>

### 打包

在上述配置修改完成之后，回到 **项目根目录**，即 `package.json` 所在目录，执行 `npm run build` 即可打包 `client`和`server` 端代码，放入根目录下的 `dist` 目录。

### 运行

将 **打包** 步骤生成的 `dist` 目录，拷贝到最终的服务运行目录，在服务运行根目录，执行 `sh tools/server_constrol.sh reload production` 即可重启服务