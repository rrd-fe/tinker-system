/**
 * 应用入口
 * Created by Jess on 2018/6/5.
 */

'use strict';

const path = require('path');
const leek = require('leekjs');
const os = require('os');

//一些配置项，放在 ~/.tinker.env 文件里
const result = require('dotenv').config({
    path: path.normalize(`${os.homedir()}${path.sep}.tinker.env`),
});

if( result.error ){
    console.error(`解析 ~/.tinker.env 异常！\n`, result.error);
}

const LeekApp = leek.LeekApp;

const app = new LeekApp({
    appRoot: __dirname,
    port: 9030,
    prefix: '/'
});

app.run();


