/**
 * 通过命令行登录、发布等操作的API接口
 */

'use strict';

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

//cli配置
const cliConfig = leek.getConfig( 'cli');

const SECRET = cliConfig.secret;
const expiresIn = cliConfig.expiresIn;

const Controller = require('../base/publish_base.js');

class CliController extends Controller{

    /**
     * 登录接口
     */
    async loginAction(){
        const {ctx} = this;
        const body = ctx.request.body;

        const {password,userName} = body;

        const result = await ctx.callService('user.getUserInfoByName',userName);

        if(result){
            //登录成功
            let oldPassword = result.pwd || '';

            let validate = bcrypt.compareSync(password, oldPassword);

            //密码是否正确
            if(validate){

                if(result.status === 2 ){
                    return this.error('用户被禁用');
                }
                
                const payload = {
                    userId: result.id,
                };
                const token = jwt.sign(payload, SECRET, { expiresIn: expiresIn});
                ctx.cliToken = token;

                this.ok({
                    name: result.name
                });
                return;
            }else{
                this.error('密码错误');
            }
        }else{
            this.error('用户不存在');
        }
    }

    /**
     * 当前登录的用户信息
     */
    async currentUserAction(){
        let user = null;
        if( this.ctx.user ){
            user = Object.assign({}, this.ctx.user);
            delete user.pwd;
        }
        this.ok({
            user: user
        });
    }

    /**
     * 当前登录用户，有 写 权限的APP列表
     */
    async relateAppsAction(){
        const ctx = this.ctx;
        const App = ctx.app.model.App;
        const UserApp = ctx.app.model.UserApp;
        const user = ctx.user;

        //用户有 写 权限的APP ID列表
        let writableIdList = [];
        let writableAppList = [];
        try{
            writableIdList = await UserApp.findByUserIdWithAccess(user.id, UserApp.ACCESS.WRITE);
            writableIdList = writableIdList.map(function(obj){
                return obj.appId;
            });
            writableAppList = await App.findByAppIdList(writableIdList);
        }catch(err){
            this.log.error(`[dash.cli.relateApps]查找当前用户可读写的app列表异常 userId[${user.id}] 错误信息：${err.message}`); 
            return this.error('获取写权限的app列表异常');
        }

        //用户拥有的APP ID列表
        let ownAppList = [];
        try{
            ownAppList = await App.findByOwnerId(user.id);
        }catch(err){
            this.log.error(`[dash.cli.relateApps]查找当前用户拥有的的app列表异常 userId[${user.id}] 错误信息：${err.message}`);
            return this.error('获取拥有的app列表异常');
        }

        this.ok({
            own: ownAppList,
            write: writableAppList,
        });
    }

    /**
     * 获取某个 appKey 对应的详情数据
     * 在下载全量包命令中，会先查询APP信息
     * 因为任何人都可以调用这个接口，返回的字段中，只返回必须的几个字段
     */
    async appDetailAction(){
        const { ctx } = this;
        const App = ctx.app.model.App;
        const appKey = ctx.query.appKey;

        if( ! appKey ){
            this.error('appKey 不能为空');
            return;
        }

        //获取app详情
        let app = null;
        try{
            app = await App.findByAppKey(appKey);
        }catch(err){
            this.log.error(`[dash.cli.appDetailAction]查找app详情异常 appKey[${appKey}] 错误信息: ${err.message}`);
        }

        if( ! app ){
            this.error(`未找到appKey[${appKey}]对应的应用！`);
            return;
        }

        this.ok({
            id: app.id,
            appKey: app.appKey,
            name: app.name,
            entryFile: app.entryFile,
            platform: app.platform,
            platformName: app.platformName,
        });
    }

    /**
     * 通过上传源代码方式，发版
     * appId  appVersion md5 
     */
    async publishBySourceAction(){
        const ctx = this.ctx;

        const uploadObj = await this.ctx.parseUpload();

        const files = uploadObj.files || {};
        const body = uploadObj.fields || {};

        //上传的源代码File对象
        const sourceFile = files.sourceFile;
        const md5 = body.md5;

        if( ! sourceFile ){
            return this.error(`上传文件不存在`);
        }

        if( ! md5 || sourceFile.hash !== md5 ){
            //校验上传之后文件的 md5
            return this.error(`md5参数缺失或者上传文件md5校验失败`);
        }

        //将参数写入到 request.body 
        Object.assign(ctx.request.body, {
            appVersion: body.appVersion,
            uploadSourcePath: sourceFile.path,
            uploadSourceMd5: md5,
        });

        await this.doPublish();
    }

    /**
     * 通过cli在用户本地调用 react-native bundle产出的全量包上传方式发版
     */
    async publishByBundlePackageAction(){
        const ctx = this.ctx;

        const uploadObj = await this.ctx.parseUpload();

        const files = uploadObj.files || {};
        const body = uploadObj.fields || {};

        //上传的源代码File对象
        const zipFile = files.zipFile;
        const md5 = body.md5;

        if( ! zipFile ){
            return this.error(`上传文件不存在`);
        }

        if( ! md5 || zipFile.hash !== md5 ){
            //校验上传之后文件的 md5
            return this.error(`md5参数缺失或者上传文件md5校验失败`);
        }

        //将参数写入到 request.body 
        Object.assign(ctx.request.body, {
            appVersion: body.appVersion,
            uploadFullPackagePath: zipFile.path,
            uploadFullPackageMd5: md5,
        });

        await this.doPublish();
    }
}


module.exports = CliController;

