/**
 * 回滚数据统计model
 */

'use strict';

const TB_NAME = 'tb_metrics_rollback';

module.exports = function(mysqlClient){

    class MetricsRollback{

        /**
         * 
         * @param {int} appId 后台应用的id
         * @param {string} appVersion native对应的版本号
         */
        static async findByAppVersion(appId, appVersion){
            let result = await mysqlClient.select(TB_NAME, {
                where: {
                    appId,
                    appVersion,
                },
                orders: [
                    ['packageVersion', 'desc']
                ]
            });
            return result.results.map( function(obj){
                return new MetricsRollback(obj);
            });
        }

        /**
         * 
         * @param {int} appId 应用ID
         * @param {string} appVersion native版本号
         * @param {int} packageVersion 全量包版本号
         */
        static async findByPackageVersion(appId, appVersion, packageVersion){
            let result = await mysqlClient.select(TB_NAME, {
                where: {
                    appId,
                    appVersion,
                    packageVersion,
                }
            });
            return result.results.map( function(obj){
                return new MetricsRollback(obj);
            });
        }

        constructor(args){
            this.id = args.id;
            this.appId = args.appId;
            this.appVersion = args.appVersion;
            this.packageVersion = args.packageVersion;
            this.fromPackageVersion = args.fromPackageVersion;
            this.rollbackCount = args.rollbackCount || 0;
            this.createdAt = args.createdAt;
            this.updatedAt = args.updatedAt;
        }

    }

    return MetricsRollback;

};