/**
 * RN回滚的详细日志model
 */

'use strict';

const TB_NAME = 'tb_rollback_log';

module.exports = function(mysqlClient){

    class RollbackLog{

        /**
         * 
         * @param {int} appId 后台应用的id
         * @param {string} appVersion native对应的版本号
         */
        static async findByAppVersion(appId, appVersion){
            let result = await mysqlClient.select(TB_NAME, {
                where: {
                    appId,
                    appVersion,
                },
                orders: [
                    ['packageVersion', 'desc']
                ]
            });
            return result.results.map( function(obj){
                return new RollbackLog(obj);
            });
        }

        /**
         * 
         * @param {int} appId 应用ID
         * @param {string} appVersion native版本号
         * @param {int} packageVersion 全量包版本号
         */
        static async findByPackageVersion(appId, appVersion, packageVersion){
            let result = await mysqlClient.select(TB_NAME, {
                where: {
                    appId,
                    appVersion,
                    packageVersion,
                },
                orders: [
                    ['fromPackageVersion', 'desc']
                ]
            });
            return result.results.map( function(obj){
                return new RollbackLog(obj);
            });
        }

        constructor(args){
            this.id = args.id;
            this.appId = args.appId;
            this.appVersion = args.appVersion;
            this.fromPackageVersion = args.fromPackageVersion;
            this.packageVersion = args.packageVersion;
            this.msg = args.msg;
            this.extra = args.extra;
            this.createdAt = args.createdAt;
            this.updatedAt = args.updatedAt;
        }

    }

    return RollbackLog;

};