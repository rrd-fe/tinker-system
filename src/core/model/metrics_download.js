/**
 * 下载次数统计model
 */

'use strict';

const TB_NAME = 'tb_metrics_download';

module.exports = function(mysqlClient){

    class MetricsDownload{

        /**
         * 
         * @param {int} appId 后台应用的id
         * @param {string} appVersion native对应的版本号
         */
        static async findByAppVersion(appId, appVersion){
            let result = await mysqlClient.select(TB_NAME, {
                where: {
                    appId,
                    appVersion,
                },
                orders: [
                    ['packageVersion', 'desc']
                ]
            });
            return result.results.map( function(obj){
                return new MetricsDownload(obj);
            });
        }

        /**
         * 
         * @param {int} appId 应用ID
         * @param {string} appVersion native版本号
         * @param {int} packageVersion 全量包版本号
         */
        static async findOne(appId, appVersion, packageVersion){
            let result = await mysqlClient.select(TB_NAME, {
                where: {
                    appId,
                    appVersion,
                    packageVersion,
                }
            });
            if( ! result.results[0] ){
                return null;
            }
            return new MetricsDownload(result.results[0]);
        }

        constructor(args){
            this.id = args.id;
            this.appId = args.appId;
            this.appVersion = args.appVersion;
            this.packageVersion = args.packageVersion;
            this.fullDownloadCount = args.fullDownloadCount || 0;
            this.patchDownloadCount = args.patchDownloadCount || 0;
            this.createdAt = args.createdAt;
            this.updatedAt = args.updatedAt;
        }

        get totalDownloadCount(){
            return this.fullDownloadCount + this.patchDownloadCount;
        }

        toJSON(){
            return {
                id: this.id,
                appId: this.appId,
                appVersion: this.appVersion,
                packageVersion: this.packageVersion,
                fullDownloadCount: this.fullDownloadCount,
                patchDownloadCount: this.patchDownloadCount,
                totalDownloadCount: this.totalDownloadCount,
                createdAt: this.createdAt,
                updatedAt: this.updatedAt,
            };
        }

    }

    return MetricsDownload;

};