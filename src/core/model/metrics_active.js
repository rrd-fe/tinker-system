/**
 * 激活数据统计model
 */

'use strict';

const TB_NAME = 'tb_metrics_active';

module.exports = function(mysqlClient){

    class MetricsActive{

        /**
         * 
         * @param {int} appId 后台应用的id
         * @param {string} appVersion native对应的版本号
         */
        static async findByAppVersion(appId, appVersion){
            let result = await mysqlClient.select(TB_NAME, {
                where: {
                    appId,
                    appVersion,
                },
                orders: [
                    ['packageVersion', 'desc']
                ]
            });
            return result.results.map( function(obj){
                return new MetricsActive(obj);
            });
        }

        /**
         * 
         * @param {int} appId 应用ID
         * @param {string} appVersion native版本号
         * @param {int} packageVersion 全量包版本号
         */
        static async findOne(appId, appVersion, packageVersion){
            let result = await mysqlClient.select(TB_NAME, {
                where: {
                    appId,
                    appVersion,
                    packageVersion,
                }
            });
            if( ! result.results[0] ){
                return null;
            }
            return new MetricsActive(result.results[0]);
        }

        constructor(args){
            this.id = args.id;
            this.appId = args.appId;
            this.appVersion = args.appVersion;
            this.packageVersion = args.packageVersion;
            this.activeCount = args.activeCount || 0;
            this.createdAt = args.createdAt;
            this.updatedAt = args.updatedAt;
        }

    }

    return MetricsActive;

};