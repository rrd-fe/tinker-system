
'use strict';

const path = require('path');
const fse = require('fs-extra');

//生成包的根目录
const packageRoot = `/home/admin/packages`;
//日志根目录
const logRoot = `/home/admin/packages/tinker-log`;

const config = {
    //全量包的存放根目录
    packageDir : `${packageRoot}/full`,
    //增量包的存放根目录
    diffDir : `${packageRoot}/diff`,
    //app发版的日志存放根目录
    publishLogDir : `${logRoot}/publish_logs`,
    
};

config.log = {
    streams: [
        {
            level: 'info',
            path: `${logRoot}/app-log/info.log`
        },
        {
            level: 'warn',
            path: `${logRoot}/app-log/warn.log`
        },
        {
            level: 'error',
            path: `${logRoot}/app-log/error.log`
        }
    ]
};

//确保相应目录存在
fse.ensureDirSync(config.packageDir);
fse.ensureDirSync(config.diffDir);
fse.ensureDirSync(config.publishLogDir);

config.mysql = {

    connectionLimit: 10,
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'tinker_system',
    charset: 'utf8mb4',
    timezone: 'local',
    connectTimeout: 10000

};

config.redis = {
    //是否是集群模式
    isCluster: false,
    //单机模式的redis配置
    options: {
        port: 6379,          // Redis port
        host: '127.0.0.1',   // Redis host
        family: 4,           // 4 (IPv4) or 6 (IPv6)
        db: 0
    },
  };

//配置通过 cli操作的 相关参数
config.cli = {
    //jwt认证中的secret
    secret: 'HrCGYSZZbOKFYBwa',
    //jwt过期时间
    expiresIn: '7d'
};


module.exports = config;