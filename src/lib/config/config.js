/**
 *
 * Created by Jess on 2018/6/5.
 */

'use strict';

const path = require('path');
const fse = require('fs-extra');

const redisHost = require('./common/redis_host.js');

const config = {
    //离线任务的bin目录
    binDir : path.join( leek.appRoot, `bin`),
};

//日志
config.log = {
    name: 'tinker-system',
    streams: [
        {
            level: 'info',
            path: '/opt/app/node-logs/tinker-system/log/info.log'
        },
        {
            level: 'warn',
            path: '/opt/app/node-logs/tinker-system/log/warn.log'
        },
        {
            level: 'error',
            path: '/opt/app/node-logs/tinker-system/log/error.log'
        }
    ]
};

//覆盖框架默认的中间件列表
config.middleware = [
    {
        name: 'leek_meta',
    },
    {
        name: 'leek_static',
    },
    {
        package: 'koa-bodyparser',
        options: {

        }
    },
    {
      name: 'leek_session'
    }

];

//覆盖默认的中间件配置
config.middlewareOption = {
    leek_static: {
        '$mountPath': '/static',
        root: path.normalize(`${leek.appRoot}/static/`)
    },

    //session
    leek_session: {
        keys : [ 'IT2UzjcIEk8IOUIK', '0znsI81sRgyGR04L'],
        key: 'sess',
        redisPrefix: 'sess:'
    }
};

//rewrite
config.rewrite = [
    {
        match: '/',
        rewrite: '/dash/index/index'
    },
    // {
    //     match: '/p/:name/:age',
    //     rewrite: '/passport/index/hello'
    // },
    // {
    //     match: /^\/p2\/([^\/]+)\/([^\/]+)/,
    //     rewrite: '/passport/index/state?q1=$1&q2=$2'
    // }
];

//模板相关配置
config.view = {

    defaultExtension: '.nj',

    engines: [
        {
            name: 'nunjucks',
            engine: require('leek-view-nunjucks'),
            options: {
                customFunction: require('../../core/view/nunjucks.js'),
            }
        }
    ],

    engineOptions: {

        nunjucks: {

            rootDir: path.normalize(`${leek.appRoot}/views`),
            tags: {
                blockStart: '{%',
                blockEnd: '%}',
                variableStart: '{{',
                variableEnd: '}}',
                commentStart: '{#',
                commentEnd: '#}'
            }
        }
    },

    mapping: {
        '.tpl': 'nunjucks'
    }
};

//后端服务的配置
config.backend = {


};

config.mysql = {

};

config.redis = {

};

//关闭URL美化
config.urlBeautify = {
    controller: '',
    action: ''
};

//配置通过 cli操作的 相关参数
config.cli = {
    //jwt认证中的secret
    secret: 'jp2jWRuf9wnydF0Z',
    //jwt过期时间
    expiresIn: '7d'
};

//通过上传源代码发版，上传配置
config.upload = {
    //文件最大大小，字节
    maxFileSize: 200 * 1024 * 1024
};


module.exports = config;


