/**
 * 扩展框架默认的 Context 对象
 * Created by Jess on 2018/6/5.
 */

'use strict';

const jwt = require('jsonwebtoken');
const formidable = require('formidable');

//cli配置
const cliConfig = leek.getConfig( 'cli');

const SECRET = cliConfig.secret;

const USER_KEY = Symbol('user');
const UPLOAD_KEY = Symbol('ctx.upload');

//cli接口中，jwt token 在header中的名字
const CLI_TOKEN_HEADER = 'x-tinker-token';

//上传相关配置
const uploadConfig = leek.getConfig('upload');

module.exports = {

    get clientInfo() {
        return {
            ua: this.header['user-agent'],
            ip: this.ip
        };
    },

    json(data) {
        this.body = data;
    },

    error(msg) {
        this.json({
            status: -1,
            message: msg,
        });
    },

    ok(data, msg) {
        this.json({
            status: 0,
            message: msg || 'ok',
            data: data
        });
    },

    /**
     * 判断当前请求，是否是 Ajax 的
     * @return {boolean}
     */
    get xhr(){
        return this.header['x-requested-with'] === 'XMLHttpRequest';
    },

    //初始化 当前的用户信息
    async initUser() {

        const User = this.app.model.User;
        let userId = this.session.userId || '';

        let result = null;
        if (userId) {
            result = await User.findById(userId);
        }

        this.assign({"user": result});
        this[USER_KEY] = result;
    },

    get cliToken(){
        return this.header[CLI_TOKEN_HEADER] || '';
    },

    set cliToken(val){
        this.set(CLI_TOKEN_HEADER, val);
    },

    //通过 cli 发起请求，读取请求中的用户登录态
    async initCliUser(){
        const User = this.app.model.User;
        try{
            const token = this.cliToken;
            if( token ){
                const payload = jwt.verify(token, SECRET);
                if( payload.userId ){
                    const user = await User.findById(payload.userId);
                    this.assign({
                        user: user
                    });
                    this[USER_KEY] = user;
                }
            }
        }catch(err){
            this.log.error(`ctx.initCliUser异常: ${err.message}`);
        }
    },

    //获取用户信息
    get user() {
        return this[USER_KEY];
    },

    /**
     * 解析上传的文件数据
     * @returns {Promise<Object>}
     */
    async parseUpload() {
        if( this[UPLOAD_KEY]){
            return this[UPLOAD_KEY];
        }
        return new Promise((resolve, reject) => {
            try {
                const form = new formidable.IncomingForm();
                form.maxFileSize = uploadConfig.maxFileSize;
                form.hash = 'md5';
                // form.multiples = multiples;
                form.parse(this.req,  (err, fields, files) => {

                    if (err) {
                        return reject(err);
                    }
                    this[UPLOAD_KEY] = {
                        fields: fields,
                        files: files
                    };
                    resolve(this[UPLOAD_KEY]);
                });
            } catch (e) {
                reject(e);
            }

        });
    },
};


