/**
 * 上传文件的请求中，字段可能存在 fields 上，没有在 ctx.request.body 上，
 * 解析出 fields 上的字段，覆盖到 ctx.request.body 上
 */

'use strict';

class FormDataFieldParsePolicy extends leek.Policy{
    async execute(){
        const {ctx} = this;
    
        try{
            const uploadObj = await this.ctx.parseUpload();

            const files = uploadObj.files || {};
            const body = uploadObj.fields || {};

            Object.assign(ctx.request.body, body);
        }catch(err){
            ctx.log.error(`[form_data_field_parse]解析form-data类型请求异常！${err.message}`);
            ctx.body = {
                status: -1,
                message: '系统异常'
            };
            return false;
        }

    }
}

module.exports = FormDataFieldParsePolicy;