/**
 * cli 请求的接口，登录态不走 cookie，通过header里自定义token传递
 */

'use strict';

class CliLoginFilter extends leek.Policy{
    async execute(){
        const {ctx} = this;
        await ctx.initCliUser();

        if( ! ctx.user){
            ctx.body = {
                status: 1001,
                message: '登录态已过期'
            };
            return false;
        }

    }
}

module.exports = CliLoginFilter;